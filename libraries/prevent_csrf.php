<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class prevent_CSRF {
		const TOKEN_KEY = "banshee_csrf";

		private $page = null;
		private $user = null;
		private $output = null;

		/* Constructor
		 *
		 * INPUT:  object page, object user, object output
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($page, $user, $output) {
			$this->page = $page;
			$this->user = $user;
			$this->output = $output;
		}

		/* Prevent CSRF attack
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function execute() {
			if ($this->page->module == "setup") {
				return false;
			}

			if (isset($_SESSION[self::TOKEN_KEY]) == false) {
				$_SESSION[self::TOKEN_KEY] = random_string(32);
			}

			$token = hash("sha256", $_SESSION[self::TOKEN_KEY].$this->user->username);

			$this->output->add_javascript("banshee/prevent_csrf.js");
			$this->output->run_javascript("prevent_csrf('".self::TOKEN_KEY."', '".$token."')");

			if (($_SERVER["REQUEST_METHOD"] != "POST") || $this->page->ajax_request) {
				return;
			}

			if ($_POST[self::TOKEN_KEY] == $token) {
				return;
			}

			/* CSRF attack detected
			 */
			if (isset($_SERVER["HTTP_ORIGIN"])) {
				$referer = $_SERVER["HTTP_ORIGIN"];
			} else if (isset($_SERVER["HTTP_REFERER"])) {
				$referer = $_SERVER["HTTP_REFERER"];
			} else {
				$referer = "previous visited website";
			}

			$message = "CSRF attempt via %s blocked";
			$this->output->add_system_warning($message, $referer);
			$this->user->log_action($message, $referer);
			$this->user->logout();

			$_SERVER["REQUEST_METHOD"] = "GET";
			$_GET = array();
			$_POST = array();
		}
	}
?>
