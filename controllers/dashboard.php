<?php
	class dashboard_controller extends controller {
		private $alerts = array(
				array("Failed logins",      "host",   "failed_logins"),
				array("Exploit attempts",   "host",   "exploit_attempts"),
				array("CGI errors",         "cgi",    "cgi_errors"),
				array("400 Bad request",    "server", "result_bad_request"),
				array("Client bans",        "host",   "bans"),
				array("404 Not Found",      "host",   "result_not_found"),
				array("500 Internal Error", "host",   "result_internal_error"),
				array("403 Forbidden",      "host",   "result_forbidden"));

		private function show_alert($index) {
			if (valid_input($index, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				return;
			} else if ($index >= count($this->alerts)) {
				return;
			}

			list($title, $type, $column) = $this->alerts[(int)$index];

			$cache = new cache($this->db, "dashboard_".$this->user->username);
			if (($list = $cache->$column) === NULL) {
				$function = "get_".$type."_statistics";
				$list = $this->model->$function($column);
				$cache->store($column, $list, ($this->settings->dashboard_page_refresh * 60) - 1);
			}
			
			if ($list == false) {
				return;
			}

			$this->output->open_tag("list", array("title" => $title));
			foreach ($list as $name => $item) {
				$this->output->add_tag("item", $name, array(
					"count"  => $item["today"],
					"change" => sprintf("%0.1f", $item["change"])));
			}
			$this->output->close_tag();
		}

		private function show_hiawatha_weblog() {
			if ($this->output->fetch_from_cache("weblog")) {
				return;
			}

			$hostname = "www.hiawatha-webserver.org";
			$hiawatha_website = new HTTPS($hostname);
			if (($result = $hiawatha_website->GET("/weblog?output=xml")) == false) {
				return;
			}

			if (substr($result["headers"]["content-type"], 0, 8) != "text/xml") {
				return;
			}

			$result["body"] = str_replace("&lt;img src=&quot;/", "&lt;img src=&quot;https://".$hostname."/", $result["body"]);
			$result["body"] = str_replace("&lt;a href=&quot;/", "&lt;a href=&quot;https://".$hostname."/", $result["body"]);

			$this->output->start_caching("weblog", 300);
			$this->output->add_xml($result["body"], "weblogs");
			$this->output->stop_caching();
		}

		public function execute() {
			if ($this->page->ajax_request) {
				if ($this->page->pathinfo[1] == null) {
					$this->output->add_tag("max_alert_count", count($this->alerts));
					$this->output->add_tag("page_refresh", $this->settings->dashboard_page_refresh);
				} else {
					$this->show_alert($this->page->pathinfo[1]);
				}
				return;
			}

			if (isset($_SESSION["latest_hiawatha_version"]) == false) {
				$hiawatha_website = new HTTPS("www.hiawatha-webserver.org");
				if (($result = $hiawatha_website->GET("/latest")) !== false) {
					$_SESSION["latest_hiawatha_version"] = $result["body"];
				}
			}

			if (isset($_SESSION["latest_mbedtls_version"]) == false) {
				$mbedtls_website = new HTTPS("tls.mbed.org");
				if (($result = $mbedtls_website->GET("/download/latest-stable-version")) !== false) {
					$_SESSION["latest_mbedtls_version"] = $result["body"];
				}
			}

			/* Webserver
			 */
			if (($webservers = $this->model->get_webservers()) === false) {
				return;
			}
			if (count($webservers) == 0) {
				$this->output->add_system_message("No webservers have been configured yet. ".($this->user->is_admin ? "Add them via the Webserver administration page in the CMS." : "Ask an administrator to add them for you."));
			}

			$webservers_offline = false;

			foreach ($webservers as $webserver) {
				$webserver["address"] = ($webserver["tls"] == 0 ? "http" : "https") . "://".$webserver["ip_address"];
				if ((($webserver["tls"] == 0) && ($webserver["port"] != 80)) ||
				    (($webserver["tls"] == 1) && ($webserver["port"] != 443))) {
					$webserver["address"] .= ":".$webserver["port"];
				}
				$webserver["address"] .= "/";

				if ($webserver["active"]) {
					if ($webserver["errors"] == 0) {
						$webserver["status"] = "online";
					} else {
						$webserver["status"] = "offline";
						$webservers_offline = true;
					}
				}

				$webserver["tls"] = show_boolean($webserver["tls"]);
				$webserver["active"] = show_boolean($webserver["active"]);

				if ($webserver["version"] != "") {
					$parts = explode(",", $webserver["version"]);

					$webserver["version"] = array_shift($parts);
					$webserver["modules"] = $parts;
					list(, $version) = explode("v", $webserver["version"], 2);
					$comparison = version_compare($version, $_SESSION["latest_hiawatha_version"], ">=");
					$webserver["uptodate"] = show_boolean($comparison);

					foreach ($parts as $part) {
						if (in_array(substr(ltrim($part), 0, 3), array("TLS", "SSL"))) {
							$version = trim(substr($part, 4), " (v)");
							if (version_compare($version, $_SESSION["latest_mbedtls_version"], "<")) {
								$webserver["uptodate"] .= " (mbed TLS out of date)";
							}
						}
					}
				}

				$this->output->open_tag("webserver", array("id" => $webserver["id"]));

				$this->output->record($webserver);

				if (is_array($webserver["modules"])) {
					$this->output->open_tag("modules");
					foreach ($webserver["modules"] as $module) {
						$this->output->add_tag("module", trim($module));
					}
					$this->output->close_tag();
				}

				$this->output->close_tag();
			}

			if ($webservers_offline) {
				$this->output->add_system_warning("Warning, one or more webservers are unavailable or caused errors during syncing of the statistics!");
			}

			/* Alerts
			 */
			$this->output->add_javascript("jquery/jquery-ui.js");
			$this->output->add_javascript("dashboard.js");
			$this->output->add_css("jquery/jquery-ui.css");

			$this->output->add_tag("threshold_change", $this->settings->dashboard_threshold_change);
			$this->output->add_tag("threshold_value", $this->settings->dashboard_threshold_value);
			$this->output->add_tag("page_refresh", $this->settings->dashboard_page_refresh);

			if ($this->settings->dashboard_show_weblog) {
				$this->show_hiawatha_weblog();
			}
		}
	}
?>
