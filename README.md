The Hiawatha Monitor
====================
The Hiawatha Monitor is a monitoring tool for the Hiawatha webserver. It can
be used to keep track of the performance and security of all your Hiawatha
webservers via one single interface. It's not a replacement for analytics
software or the local logfiles, but it gives you a quick and easy overview
of how your webservers are performing and which ones require some attention. 

Read the information at https://www.hiawatha-webserver.org/howto/monitor for
compilation instructions for the Hiawatha webserver.
